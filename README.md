# [Demo] Livewire Better Structure

This project is for demo of what a better structure for livewire project

# Usages

1. The project is not for demo / run in local or remote
2. The project is for documentation and explanation of what a better structure for livewire project

# Summary

![1703035191051](documentations/image/README/1703035191051.png)

# Improvements

**(+) What improvement on NEW structure :**

1. Optimize workload between back-end and front-end
2. Reducing I/O (Input & Output) from back-end to front-end
3. Reducing Complexity and brain surgeon of code / scrambled code
4. Reducing IO to database
5. Reusable front-end component with bunch of tooling needed to full
   control the UI
6. Changing the route of programming to better one
7. Fast and snappier UI changes, resulting in better UI/UX
8. Better for group project / collaboration project

**(-) What cons for NEW structure :**

1. A bit complex javascript code (per-component)
2. Need learn a little bit how to use the component (every component
   have their own utility)
3. Increase in size of blade view (not by much)


**(-) Why OLD one is bad ? :**

1. Back-end as Javascript (slow update and eating server I/O)
2. Cobweb code (you pull one string, unknown string will be pulled,
   because how every single component can connect to each other).
   Because how component can interact with each other, component have
   multiple responsibility instead of one (this one basically depend on
   how you structure the back-end) which resulting sometimes
   complicated code
3. Javascript almost become useless except for simple things like limit
   the input to 10, or Etc.
4. Not balance workload between back-end and front-end resulting in bad
   server performance in long run (future)
5. Too many request to back-end for single component. because single
   component operation is in the back-end. We using back-end like how
   javascript checking the value for operations, resulting in too many
   request to back-end e.g : changing status from verified to
   unverified when you change phonenumber, while you can do it in
   javascript.
6. Bad structure because how component can interact with each other, so
   component have multiple responsibility instead of one (this one
   basically depend on how you structure the back-end)

**(+) Why OLD one is good :**

1. Fast development (because consist only back-end /livewire, no need
   to code javascript except little)
2. Good for solo / alone developer if they want fast website
